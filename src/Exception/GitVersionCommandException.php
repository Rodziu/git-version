<?php
/**
 *    _______ __     _    __               _
 *   / ____(_) /_   | |  / /__  __________(_)___  ____
 *  / / __/ / __/   | | / / _ \/ ___/ ___/ / __ \/ __ \
 * / /_/ / / /_     | |/ /  __/ /  (__  ) / /_/ / / / /
 * \____/_/\__/     |___/\___/_/  /____/_/\____/_/ /_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\Exception;

/**
 * Class GitVersionCommandException
 * @package Rodziu\Exception
 */
class GitVersionCommandException extends \RunTimeException{
	const UNRECOGNIZED_OPTION = 1;
	const OPTION_AFTER_COMMAND = 2;
	const NO_SUCH_COMMAND = 3;
	const NO_SUCH_DIRECTORY = 4;

	/**
	 * GitHelperException constructor.
	 *
	 * @param string $message
	 * @param int $code
	 * @param \Throwable $previous
	 */
	public function __construct(string $message = "", int $code = 0, \Throwable $previous = null){
		parent::__construct($message, $code, $previous);
	}
}