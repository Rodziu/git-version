<?php
/**
 *    _______ __     _    __               _
 *   / ____(_) /_   | |  / /__  __________(_)___  ____
 *  / / __/ / __/   | | / / _ \/ ___/ ___/ / __ \/ __ \
 * / /_/ / / /_     | |/ /  __/ /  (__  ) / /_/ / / / /
 * \____/_/\__/     |___/\___/_/  /____/_/\____/_/ /_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\Exception;

/**
 * Class GitVersionException
 * @package Rodziu\Exception
 */
class GitVersionException extends \RunTimeException{
	const DETACHED_HEAD_STATE = 1;
	const GIT_VERSION_ALREADY_SET = 2;

	/**
	 * GitHelperException constructor.
	 *
	 * @param string $message
	 * @param int $code
	 * @param \Throwable $previous
	 */
	public function __construct(string $message = "", int $code = 0, \Throwable $previous = null){
		parent::__construct($message, $code, $previous);
	}
}