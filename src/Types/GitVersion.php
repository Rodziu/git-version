<?php
/**
 *    _______ __     _    __               _
 *   / ____(_) /_   | |  / /__  __________(_)___  ____
 *  / / __/ / __/   | | / / _ \/ ___/ ___/ / __ \/ __ \
 * / /_/ / / /_     | |/ /  __/ /  (__  ) / /_/ / / / /
 * \____/_/\__/     |___/\___/_/  /____/_/\____/_/ /_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\Types;

use Rodziu\GenericTypes\GenericStructure;

/**
 * Class GitVersion
 * @package Rodziu\Types
 */
class GitVersion extends GenericStructure{
	/**
	 * @var string
	 */
	public $version = '';
	/**
	 * @var int
	 */
	public $build = '';

	/**
	 * GitVersion constructor.
	 *
	 * @param string $version
	 * @param int $build
	 */
	public function __construct(string $version = '', int $build = 0){
		$this->version = $version;
		$this->build = $build;
	}

	/**
	 * @param string $json
	 *
	 * @return mixed
	 */
	public static function fromJson(string $json): GitVersion{
		return self::fromArray(json_decode($json, true));
	}

	/**
	 * @return string
	 */
	public function toJson(): string{
		return json_encode($this->toArray(), JSON_PRETTY_PRINT);
	}

	/**
	 * @return string
	 */
	public function __toString(): string{
		return $this->version.' build '.$this->build;
	}
}