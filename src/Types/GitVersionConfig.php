<?php
/**
 *    _______ __     _    __               _
 *   / ____(_) /_   | |  / /__  __________(_)___  ____
 *  / / __/ / __/   | | / / _ \/ ___/ ___/ / __ \/ __ \
 * / /_/ / / /_     | |/ /  __/ /  (__  ) / /_/ / / / /
 * \____/_/\__/     |___/\___/_/  /____/_/\____/_/ /_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\Types;

use Rodziu\GenericTypes\GenericStructure;
use Rodziu\GitVersion\Command;

/**
 * Class GitVersionConfig
 * @package Rodziu\Types
 */
class GitVersionConfig extends GenericStructure{
	/**
	 * @var string
	 */
	public $versionFile = '';
	/**
	 * @var bool
	 */
	public $createChangelog = true;
	/**
	 * @var string
	 */
	public $changelogFile = '';
	/**
	 * @var string
	 */
	public $dateFormat = '';
	/**
	 * @var string
	 */
	public $dateLanguage = '';
	/**
	 * @var string
	 */
	public $changelogFormat = '';
	/**
	 * @var bool
	 */
	public $newlinesAsLists = true;

	/**
	 * GitVersionConfig constructor.
	 *
	 * @param string|null $versionFile
	 * @param bool $createChangelog
	 * @param string|null $changelogFile
	 * @param string|null $dateFormat
	 * @param string $dateLanguage
	 * @param string|null $changelogFormat
	 * @param bool $newlinesAsLists
	 */
	public function __construct(
		string $versionFile = null, bool $createChangelog = true, string $changelogFile = null,
		string $dateFormat = null, string $dateLanguage = 'pl', string $changelogFormat = null,
		bool $newlinesAsLists = true
	){
		$this->versionFile = $versionFile ?? "version.json";
		$this->createChangelog = $createChangelog;
		$this->changelogFile = $changelogFile ?? "changelog.md";
		$this->dateFormat = $dateFormat ?? "j F Y H:i";
		$this->dateLanguage = $dateLanguage;
		$this->changelogFormat = $changelogFormat ?? "## {version} <small>{authors}, {date}</small>\n\n{text}";
		$this->newlinesAsLists = $newlinesAsLists;
	}

	/**
	 * @param string $json
	 *
	 * @return mixed
	 */
	public static function fromJson(string $json): GitVersionConfig{
		return self::fromArray(json_decode($json, true));
	}

	/**
	 * @return string
	 */
	public function toJson(): string{
		return json_encode($this->toArray(), JSON_PRETTY_PRINT);
	}

	/**
	 * @param GitVersionConfig|null $currentConfig
	 *
	 * @return GitVersionConfig
	 */
	public static function createInteractively(GitVersionConfig $currentConfig = null): self{
		$config = $currentConfig === null ? new self : $currentConfig;
		$setProperty = function(string $property, string $type = 'string') use (&$config){
			$cin = Command::cin();
			if(!empty($cin)){
				if($type == 'bool'){
					$cin = $cin === 'true' || $cin === '1';
				}
				$config->$property = $cin;
			}
		};
		echo "version.json file name [\e[34m{$config->versionFile}\e[0m]: ";
		$setProperty('versionFile');
		echo "Enable changelog (true/false) [\e[34m".($config->createChangelog ? 'true' : 'false')."\e[0m]: ";
		$setProperty('createChangelog', 'bool');
		echo "changelog.md file name [\e[34m{$config->changelogFile}\e[0m]: ";
		$setProperty('changelogFile');
		echo "Date format for generated changelog entries [\e[34m{$config->dateFormat}\e[0m]: ";
		$setProperty('dateFormat');
		echo "Date language for generated changelog entries [\e[34m{$config->dateLanguage}\e[0m]: ";
		$setProperty('dateLanguage');
		echo "Changelog format (for each change block). You can use \e[92m{date} {text}\e[0m and \e[92m{authors}\e[0m blocks.".PHP_EOL
			."\e[92m{version}\e[0m block is required to detect versions already contained in changelog.md".PHP_EOL
			."[\e[34m"
			.str_replace("\n", "\\n", $config->changelogFormat)
			."\e[0m]: ";
		$setProperty('changeLogFormat');
		echo "Turn multi line commit messages in markdown lists (true/false) [\e[34m".
			($config->newlinesAsLists ? 'true' : 'false')."\e[0m]: ";
		$setProperty('newlinesAsLists', 'bool');
		return $config;
	}
}