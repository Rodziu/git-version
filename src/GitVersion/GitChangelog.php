<?php
/**
 *    _______ __     _    __               _
 *   / ____(_) /_   | |  / /__  __________(_)___  ____
 *  / / __/ / __/   | | / / _ \/ ___/ ___/ / __ \/ __ \
 * / /_/ / / /_     | |/ /  __/ /  (__  ) / /_/ / / / /
 * \____/_/\__/     |___/\___/_/  /____/_/\____/_/ /_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GitVersion;

use Rodziu\DateTimeLocalized\Config;
use Rodziu\Git\GitRepository;
use Rodziu\Git\Types\Commit;
use Rodziu\Git\Types\Tag;
use Rodziu\Types\GitVersionConfig;

/**
 * Class GitChangelog
 * @package Rodziu\GitVersion
 */
class GitChangelog{
	/**
	 * @var string
	 */
	private $workingDirectory = "";
	/**
	 * @var GitRepository
	 */
	private $gitRepository;
	/**
	 * @var GitVersionConfig
	 */
	private $config;

	/**
	 * GitChangelog constructor.
	 *
	 * @param string $workingDirectory
	 * @param GitRepository $gitRepository
	 * @param GitVersionConfig $config
	 */
	public function __construct(string $workingDirectory, GitRepository $gitRepository, GitVersionConfig $config){
		$this->workingDirectory = $workingDirectory;
		$this->gitRepository = $gitRepository;
		$this->config = $config;
		Config::setLocale($this->config->dateLanguage);
	}

	/**
	 * @return bool
	 */
	public function checkBranch(): bool{
		return $this->gitRepository->getHead()->branch == 'master';
	}

	/**
	 * @param bool $fromLatestCommit
	 *
	 * @return bool
	 */
	public function hasNewChanges(bool $fromLatestCommit = false): bool{
		$lastTag = $this->getLastTagInChangelog();
		return !empty($this->getChangelogFromCommits($lastTag, $fromLatestCommit ? '' : null));
	}

	/**
	 * @param string|null $newVersion
	 *
	 * @return bool
	 */
	public function updateChangelog(string $newVersion = null): bool{
		$lastTag = $this->getLastTagInChangelog();
		// get new changes
		$changelog = $this->getChangelogFromCommits($lastTag, $newVersion);
		foreach($changelog as &$c){
			$c = $this->formatChangelog($c);
		}
		unset($c);
		$changelog = implode(PHP_EOL.PHP_EOL, $changelog);
		// write to changelog.md
		if(!empty($changelog)){
			$umask = umask(0);
			$changelogPath = $this->workingDirectory.DIRECTORY_SEPARATOR.$this->config->changelogFile;
			$tmpFilePath = tempnam(sys_get_temp_dir(), 'gitv-changelog');
			register_shutdown_function(function() use ($tmpFilePath){
				@unlink($tmpFilePath);
			});
			$tmpFile = fopen($tmpFilePath, 'w+');
			fwrite($tmpFile, $changelog);
			if(file_exists($changelogPath)){
				fwrite($tmpFile, PHP_EOL.PHP_EOL);
				$file = fopen($changelogPath, 'r');
				while(($line = fgets($file)) !== false){
					fwrite($tmpFile, $line);
				}
				fclose($file);
			}
			fclose($tmpFile);
			copy($tmpFilePath, $changelogPath);
			umask($umask);
			return true;
		}
		return false;
	}

	/**
	 * @return null|string
	 */
	public function getLastTagInChangelog(){
		$lastTag = null;
		// parse changelog.md
		$changelogPath = $this->workingDirectory.DIRECTORY_SEPARATOR.$this->config->changelogFile;
		if(file_exists($changelogPath)){
			$tags = $this->gitRepository->getTags()->toArray(false);
			$file = fopen($changelogPath, 'r');
			while(($line = fgets($file)) !== false){
				if(empty($tags)){
					break;
				}
				foreach($tags as $k => $tag){
					/** @var $tag Tag */
					if(stripos($line, $tag->tag) !== false){
						unset($tags[$k]);
						$lastTag = $tag->tag;
						break 2;
					}
				}
			}
			fclose($file);
		}
		return $lastTag;
	}

	/**
	 * @param null|string $afterTag
	 *
	 * @param string|null $newVersion
	 *
	 * @return array
	 */
	public function getChangelogFromCommits(?string $afterTag, string $newVersion = null): array{
		$commits = $this->gitRepository->getHistory();
		$tags = array_column($this->gitRepository->getTags()->toArray(), 'tag', 'commit');
		$tag = null;
		$stack = $results = [];
		/** @var $commit Commit */
		foreach($commits as $k => $commit){
			if(isset($tags[$commit->commitHash])){ // tagged commit
				if($tag !== null && !empty($stack['text'])){
					$results[] = $stack;
				}
				$tag = $tags[$commit->commitHash];
				$stack = [
					'version' => $tag,
					'authors' => [],
					'text'    => [],
					'date'    => $commit->authorDate
				];
			}else if($k === 0 && $newVersion !== null){ // treat first commit as a tagged one
				$tag = $newVersion;
				$stack = [
					'version' => $tag,
					'authors' => [],
					'text'    => [],
					'date'    => $commit->authorDate
				];
			}
			if($tag === null){
				continue; // do nothing with not-versioned (not tagged) commits
			}else if($afterTag !== null && $tag == $afterTag){
				break;
			}
			$stack['text'] = array_merge($stack['text'], explode(PHP_EOL, $commit->message));
			if(!in_array($commit->authorName, $stack['authors'])){
				$stack['authors'][] = $commit->authorName;
			}
		}
		if($tag !== null && !empty($stack['text'])){
			$results[] = $stack;
		}
		return $results;
	}

	/**
	 * @param array $changelog
	 *
	 * @return string
	 */
	public function formatChangelog(array $changelog): string{
		$ret = $this->config->changelogFormat;
		foreach($changelog as $k => $v){
			switch($k){
				case 'authors':
					$v = implode(', ', $v);
					break;
				case 'text':
					$lineStart = $this->config->newlinesAsLists ? '- ' : '';
					$v = $lineStart.implode(PHP_EOL.$lineStart, $v);
					break;
				case 'date':
					/** @var \DateTimeImmutable $v */
					$v = $v->format($this->config->dateFormat);
					break;
				default:
					break;
			}
			$ret = str_replace('{'.$k.'}', $v, $ret);
		}
		return $ret;
	}
}