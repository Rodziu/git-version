<?php
/**
 *    _______ __     _    __               _
 *   / ____(_) /_   | |  / /__  __________(_)___  ____
 *  / / __/ / __/   | | / / _ \/ ___/ ___/ / __ \/ __ \
 * / /_/ / / /_     | |/ /  __/ /  (__  ) / /_/ / / / /
 * \____/_/\__/     |___/\___/_/  /____/_/\____/_/ /_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GitVersion;

use Rodziu\DateTimeLocalized\DateTimeImmutable;
use Rodziu\Exception\GitVersionCommandException;
use Rodziu\Git\GitRepository;
use Rodziu\Types\GitVersion;
use Rodziu\Types\GitVersionConfig;

/**
 * Class Command
 * @package Rodziu\GitVersion
 */
class Command{
	const COMMANDS = ['init', 'install-hooks', 'config', 'version', 'changelog'];
	/**
	 * @var string
	 */
	private $command = "";
	/**
	 * @var array
	 */
	private $args = [];
	/**
	 * @var string
	 */
	private $workingDirectory = "";
	/**
	 * @var GitRepository
	 */
	private $gitRepository;
	/**
	 * @var GitVersionConfig
	 */
	private $config;
	/**
	 * @var GitVersion
	 */
	public $version;

	/**
	 * Command constructor.
	 */
	public function __construct(){
		$json = @file_get_contents(dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.'version.json');
		if($json == false){
			$json = '{}';
		}
		$this->version = GitVersion::fromJson($json);
	}

	/**
	 */
	public static function main(){
		$cmd = new static;
		try{
			echo "\033[1m\033[92mGit-Version {$cmd->version->version} by Mateusz Rohde.\033[0m".PHP_EOL.PHP_EOL;
			if(
				php_sapi_name() !== 'cli'
				|| ($envTmpFile = getenv('GITV_ENV')) === false
			){
				self::displayError(
					"Git-Version needs to be executed by gitv script located at: ".PHP_EOL
					.__DIR__.DIRECTORY_SEPARATOR.'gitv'.PHP_EOL
					.'or at vendor/bin/gitv'
				);
			}else{
				$cmd->initialize($_SERVER['argv']);
				if($cmd->config === false && $cmd->command != 'init'){
					self::displayError("Configuration file not found. Please use `gitv init` command or create it manually.");
				}
				$cmd->run($envTmpFile);
			}
		}catch(\Throwable $e){
			self::displayError($e->getMessage());
		}
	}

	/**
	 * @param string $errorMessage
	 * @param bool $exit
	 */
	public static function displayError(string $errorMessage, bool $exit = true){
		echo "\033[31m{$errorMessage}\033[0m";
		if(!preg_match("#".PHP_EOL.'$#', $errorMessage)){
			echo PHP_EOL;
		}
		if($exit){
			exit;
		}
	}

	/**
	 * @return string
	 */
	public static function cin(): string{
		$stdin = fopen('php://stdin', 'r');
		$code = trim(fgets($stdin));
		fclose($stdin);
		return $code;
	}

	/**
	 * @param array $argv
	 * @param array $options
	 */
	public function initialize(array $argv, array $options = null){
		$this->handleArguments($argv, $options);
		$this->gitRepository = new GitRepository($this->workingDirectory.DIRECTORY_SEPARATOR.".git");
		$this->config = $this->getConfig();
	}

	/**
	 * @param array $argv
	 * @param array|null $options
	 *
	 * @throws GitVersionCommandException
	 */
	private function handleArguments(array $argv, array $options = null){
		$idx = 1;
		if(is_null($options)){
			if(version_compare(PHP_VERSION, '7.1.0') >= 0){
				$options = getopt("w:h", ['working-directory:', 'help'], $idx);
			}else{
				$options = getopt("w:h", ['working-directory:', 'help']);
				foreach($argv as $k => $v){
					if($k == 0){
						continue;
					}
					if(strpos($v, '-') !== false){
						if($v == '--working-directory' || $v == '-w'){
							$idx++;
						}
						$idx++;
					}else{
						break;
					}
				}
			}
		}
		foreach($argv as $k => $v){
			if(preg_match('#^(--?)([^ ]+)#', $v, $match)){
				$option = $match[1] == '-' ? substr($match[2], 0, 1) : $match[2];
				if($idx < $k){
					throw new GitVersionCommandException(
						"Options should be given at the beginning (before commands)!",
						GitVersionCommandException::OPTION_AFTER_COMMAND
					);
				}
				if(!isset($options[$option])){
					throw new GitVersionCommandException(
						"Unrecognized option {$match[1]}{$option}", GitVersionCommandException::UNRECOGNIZED_OPTION
					);
				}
			}
		}
		// get command and args
		$command = $argv[$idx] ?? '';
		if(!empty($command) && !in_array($command, self::COMMANDS)){
			throw new GitVersionCommandException(
				"No such command $command",
				GitVersionCommandException::NO_SUCH_COMMAND
			);
		}else{
			$this->command = $command;
		}
		$this->args = array_slice($argv, $idx + 1);
		// process options
		foreach($options as $option => $value){
			switch($option){
				case 'w':
				case 'working-directory':
					if(is_dir($value)){
						$this->workingDirectory = rtrim($value, DIRECTORY_SEPARATOR);
					}else{
						throw new GitVersionCommandException(
							"No such directory $value!", GitVersionCommandException::NO_SUCH_DIRECTORY
						);
					}
					break;
				case 'h':
				case 'help':
					$this->command = 'help';
					$this->args = [];
					break;
			}
		}
		// defaults
		if(empty($this->workingDirectory)){
			$this->workingDirectory = getcwd();
		}
	}

	/**
	 * @return bool|mixed|GitVersionConfig
	 */
	private function getConfig(){
		$configPath = $this->workingDirectory.DIRECTORY_SEPARATOR.".gitv".DIRECTORY_SEPARATOR."config.json";
		if(file_exists($configPath)){
			return GitVersionConfig::fromJson(file_get_contents($configPath));
		}
		return false;
	}

	/**
	 */
	private function displayHelp(){
		// pretty print cli table
		$printTable = function(array $table){
			$maxWidth = 80;
			// set left column width to longest left col + 2 spaces on left + 4 spaces on right
			$leftWidth = max(array_map(function($item){
					return mb_strlen(trim($item), 'UTF-8');
				}, array_keys($table))) + 2 + 4;
			$rightWidth = $maxWidth - $leftWidth - 2;
			foreach($table as $l => $r){
				$l = "  ".trim($l);
				echo str_pad($l, $leftWidth, " ", STR_PAD_RIGHT);
				if(mb_strlen($l, 'UTF-8') >= $leftWidth){ // move right col to next line if left col is too long
					echo PHP_EOL.str_pad(" ", $leftWidth, " ", STR_PAD_RIGHT);
				}
				$r = preg_split("#\s#", $r);
				$len = 0;
				while(!is_null($word = array_shift($r))){
					$word = trim($word);
					if(!empty($word)){
						$wordLength = mb_strlen($word, 'UTF-8');
						if($len + $wordLength + 1 > $rightWidth){
							echo PHP_EOL.str_pad(" ", $leftWidth + 2, " ", STR_PAD_RIGHT);
							$len = $wordLength + 1;
						}else{
							$len += $wordLength + 1;
						}
						echo "$word ";
					}
				}
				echo PHP_EOL;
			}
		};
		echo /** @lang text */
			"Usage: gitv [options] <command>".PHP_EOL.PHP_EOL;
		echo "Options:".PHP_EOL;
		$options = [
			'-w, --working-directory' => 'Full path to directory where version and changelog files will be stored, this directory has to contain .git directory.',
			'-h, --help'              => 'Prints this usage information.',
		];
		$printTable($options);
		echo PHP_EOL;
		echo "Commands:".PHP_EOL;
		$commands = [
			'init'             => 'Initialize git-version, create it\'s config file.',
			'config'           => 'Get/set configuration value.',
			'version'          => 'Create a new release.',
			'changelog check'  => 'Check if there are any changes not included in changelog file.',
			'changelog update' => 'Add any new changes to changelog file.'
		];
		$printTable($commands);
		exit;
	}

	/**
	 * @param string $envTmpFile
	 */
	private function run(string $envTmpFile){
		$writeConfig = function(GitVersionConfig $config){
			echo "Writing config file... ";
			$umask = umask(0);
			@mkdir($this->workingDirectory.DIRECTORY_SEPARATOR.".gitv");
			file_put_contents(
				$this->workingDirectory.DIRECTORY_SEPARATOR.".gitv".DIRECTORY_SEPARATOR."config.json",
				$config->toJson()
			);
			umask($umask);
			echo "\e[92m"."done!\e[0m".PHP_EOL;
		};
		switch($this->command){
			case 'init':
				$config = GitVersionConfig::createInteractively($this->config === false ? null : $this->config);
				$writeConfig($config);
				break;
			case 'config':
				if(
					isset($this->args[0]) && in_array($this->args[0], ['get', 'set'])
					&& isset($this->args[1])
					&& in_array($this->args[1], array_keys($this->config->toArray()))
				){
					if($this->args[0] == 'get'){
						$cfg = $this->config->{$this->args[1]};
						if(is_bool($cfg)){
							$cfg = $cfg ? 'true' : 'false';
						}
						echo $cfg.PHP_EOL;
						break;
					}else if(isset($this->args[2])){
						if(is_bool($this->config->{$this->args[1]})){
							$this->args[2] = $this->args[2] === 'true' || $this->args[2] === '1';
						}
						$this->config->{$this->args[1]} = $this->args[2];
						$writeConfig($this->config);
						break;
					}
				}
				echo /** @lang text */
					"Usage: gitv config set <name> <value>".PHP_EOL;
				echo /** @lang text */
					"       gitv config get <name>".PHP_EOL;
				break;
			case 'version':
				$v = new Version(
					$this->workingDirectory, $this->gitRepository, $this->config
				);
				echo "Current version from {$this->config->versionFile}: {$v->getVersionInFile()->version}".PHP_EOL;
				echo "Current version from GIT: {$v->getVersion()->version}".PHP_EOL;
				do{
					echo "New version: ";
					$newVersion = self::cin();
					$status = Version::validateSemver($newVersion);
					if(!$status){
						self::displayError("Invalid semver version", false);
					}
				}while(!$status);
				$releaseDate = new DateTimeImmutable('now', new \DateTimeZone('UTC'));
				$v->updateVersionFile(
					new GitVersion($newVersion, (int)$releaseDate->format('YmdHis'))
				);
				$changelogUpdated = false;
				if($this->config->createChangelog){
					$gc = new GitChangelog(
						$this->workingDirectory, $this->gitRepository, $this->config
					);
					if($gc->checkBranch() && $gc->hasNewChanges(true)){
						echo "Do you want to update {$this->config->changelogFile} file? [y/n]: ";
						$updateChangelog = self::cin();
						if($updateChangelog === 'y' || $updateChangelog === 'Y'){
							$changelogUpdated = $gc->updateChangelog($newVersion);
						}
					}
				}
				file_put_contents(
					$envTmpFile,
					"GITV_WD='{$this->workingDirectory}'".PHP_EOL
					."GITV_CHANGELOG_EDIT=".((int)$changelogUpdated).PHP_EOL
					."GITV_VERSION_FILE=\"{$this->config->versionFile}\"".PHP_EOL
					."GITV_CHANGELOG_FILE=\"".
					($this->config->createChangelog ? " {$this->config->changelogFile}" : "")
					."\"".PHP_EOL
					."GITV_RELEASE_DATE='{$releaseDate->getTimestamp()}'".PHP_EOL
					."GITV_VERSION='$newVersion'"
				);
				break;
			case 'changelog':
				if($this->config->createChangelog){
					$gc = new GitChangelog(
						$this->workingDirectory, $this->gitRepository, $this->config
					);
					if(!$gc->checkBranch() && !(isset($this->args[1]) && $this->args[1] == 'force')){
						echo "Changelog check/update is ignored on non-master branch.".PHP_EOL;
						echo "You can force changelog check/update by typing force after check|update command."
							.PHP_EOL;
						break;
					}
					if(isset($this->args[0])){
						if($this->args[0] == 'check'){
							$hasChanges = $gc->hasNewChanges();
							if($hasChanges){
								echo "There are new changes not included in changelog file!".PHP_EOL;
							}else{
								echo "There are no new changes.".PHP_EOL;
							}
						}else if($this->args[0] == 'update'){
							echo "{$this->config->changelogFile} ".
								($gc->updateChangelog() ? 'has been updated.' : 'is already up to date.').PHP_EOL;
						}
						break;
					}
					echo "Usage: gitv changelog check [force]".PHP_EOL;
					echo "       gitv changelog update [force]".PHP_EOL;
				}else{
					echo "Changelog file generation is disabled by configuration.".PHP_EOL;
				}
				break;
			default:
				// help
				$this->displayHelp();
				break;
		}
		exit;
	}
}