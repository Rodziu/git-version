<?php
/**
 *    _______ __     _    __               _
 *   / ____(_) /_   | |  / /__  __________(_)___  ____
 *  / / __/ / __/   | | / / _ \/ ___/ ___/ / __ \/ __ \
 * / /_/ / / /_     | |/ /  __/ /  (__  ) / /_/ / / / /
 * \____/_/\__/     |___/\___/_/  /____/_/\____/_/ /_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GitVersion;

use Rodziu\Exception\GitVersionException;
use Rodziu\Git\GitRepository;
use Rodziu\Git\Types\Tag;
use Rodziu\Types\GitVersion;
use Rodziu\Types\GitVersionConfig;

/**
 * Class Version
 * @package Rodziu\GitVersion
 */
class Version{
	/**
	 * @var string
	 */
	private $workingDirectory = "";
	/**
	 * @var GitRepository
	 */
	private $gitRepository;
	/**
	 * @var GitVersionConfig
	 */
	private $config;

	/**
	 * Version constructor.
	 *
	 * @param string $workingDirectory
	 * @param GitRepository $gitRepository
	 * @param GitVersionConfig $config
	 */
	public function __construct(
		string $workingDirectory, GitRepository $gitRepository, GitVersionConfig $config
	){
		$this->workingDirectory = $workingDirectory;
		$this->gitRepository = $gitRepository;
		$this->config = $config;
	}

	/**
	 * @param string $version
	 *
	 * @return bool
	 */
	public static function validateSemver(string $version): bool{
		return (bool)preg_match(
			'#^((([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?)'
			.'(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?)$#m',
			$version
		);
	}

	/**
	 * @return GitVersion
	 * @throws GitVersionException
	 */
	public function getVersion(): GitVersion{
		$head = $this->gitRepository->getHead();
		$gitTags = $this->gitRepository->getTags();
		$version = null;
		foreach($gitTags as $tag){
			/** @var Tag $tag */
			if($tag->commit === $head->commitHash){
				$version = $tag->tag;
			}
		}
		if(is_null($version)){
			if(is_null($head->branch)){
				throw new GitVersionException(
					"You are in 'detached HEAD' state!",
					GitVersionException::DETACHED_HEAD_STATE
				);
			}
			$version = "dev-{$head->branch}";
		}
		return new GitVersion(
			$version,
			(int)$this->gitRepository->getCommit($head->commitHash)
				->authorDate->setTimezone(new \DateTimeZone('UTC'))
				->format('YmdHis')
		);
	}

	/**
	 * @return GitVersion
	 */
	public function getVersionInFile(): GitVersion{
		$versionPath = $this->workingDirectory.DIRECTORY_SEPARATOR.$this->config->versionFile;
		if(file_exists($versionPath)){
			$version = GitVersion::fromJson(trim(file_get_contents($versionPath)));
		}
		return $version ?? new GitVersion();
	}

	/**
	 * @param GitVersion|null $version
	 *
	 * @return GitVersion
	 */
	public function updateVersionFile(GitVersion $version = null): GitVersion{
		if(is_null($version)){
			$version = $this->getVersion();
		}
		file_put_contents(
			$this->workingDirectory.DIRECTORY_SEPARATOR.$this->config->versionFile,
			$version->toJson()
		);
		return $version;
	}
}