<?php
/**
 *    _______ __     _    __               _           
 *   / ____(_) /_   | |  / /__  __________(_)___  ____ 
 *  / / __/ / __/   | | / / _ \/ ___/ ___/ / __ \/ __ \
 * / /_/ / / /_     | |/ /  __/ /  (__  ) / /_/ / / / /
 * \____/_/\__/     |___/\___/_/  /____/_/\____/_/ /_/ 
 *
 * @author Rodziu <mateusz.rohde@gmail.com>                                                       
 * @copyright Copyright (c) 2017-2019. 
 */
foreach([__DIR__.'/../../autoload.php', __DIR__.'/vendor/autoload.php'] as $file){
	if(file_exists($file)){
		define('GIT_VERSION_COMPOSER_INSTALL', $file);
		break;
	}
}
unset($file);
if(!defined('GIT_VERSION_COMPOSER_INSTALL')){
	fwrite(
		STDERR,
		'You need to set up the project dependencies using Composer:'.PHP_EOL.PHP_EOL.
		'    composer install'.PHP_EOL.PHP_EOL.
		'You can learn all about Composer on https://getcomposer.org/.'.PHP_EOL
	);
	die(1);
}
/** @noinspection PhpIncludeInspection */
require GIT_VERSION_COMPOSER_INSTALL;
define('GIT_VERSION_PATH', __FILE__);
\Rodziu\GitVersion\Command::main();