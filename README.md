# GitVersion - easy semver management with PHP & GIT

Easily create version file in JSON format for PHP projects managed in GIT. Also allows You to create/update changelog file in markdown format from your GIT history.

## Prerequisites

- PHP 7.1+,
- GIT,
- composer.

## Installing

```bash
composer global require rodziu/git-version
```

You may want to add your global composer vendor/bin path to your PATH environment variable to make `gitv` command available globally.

## Usage

Run `gitv init` in your project path to create Git-Version configuration file interactively.

Run `gitv version` to create new version. It updates your version file, creates changelog (if enabled in config), and creates a new commit tagged with new semver version chosen by yourself.