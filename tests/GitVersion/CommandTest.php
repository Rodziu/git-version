<?php
/**
 *    _______ __     _    __               _
 *   / ____(_) /_   | |  / /__  __________(_)___  ____
 *  / / __/ / __/   | | / / _ \/ ___/ ___/ / __ \/ __ \
 * / /_/ / / /_     | |/ /  __/ /  (__  ) / /_/ / / / /
 * \____/_/\__/     |___/\___/_/  /____/_/\____/_/ /_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GitVersion;

use \PHPUnit\Framework\TestCase;
use Rodziu\Exception\GitVersionCommandException;
use Rodziu\TestsHelper;
use Rodziu\Types\GitVersionConfig;

/**
 * Class CommandTest
 * @package Rodziu\GitVersion
 */
class CommandTest extends TestCase{
	/**
	 * @var Command
	 */
	private $cmd;

	/**
	 */
	protected function tearDown(){
		parent::tearDown();
		@unlink(dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest'.DIRECTORY_SEPARATOR.'.gitv'.DIRECTORY_SEPARATOR.'config.json');
		@rmdir(dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest'.DIRECTORY_SEPARATOR.'.gitv');
	}

	/**
	 * @dataProvider handleArgumentsProvider
	 *
	 * @param string $command
	 * @param array|null $options
	 * @param int|null $exceptionCode
	 * @param string $parsedCommand
	 * @param array $args
	 */
	public function testHandleArguments(string $command, array $options = null, int $exceptionCode = null, string $parsedCommand = null, array $args = null){
		$this->cmd = new Command();
		if(!is_null($exceptionCode)){
			$this->expectException(GitVersionCommandException::class);
			$this->expectExceptionCode($exceptionCode);
		}
		TestsHelper::invokeMethod(
			$this->cmd, 'handleArguments',
			[
				preg_split("#\s+#", trim($command)), $options
			]
		);
		if(!is_null($parsedCommand)){
			$this->assertAttributeEquals($parsedCommand, 'command', $this->cmd);
		}
		if(!is_null($args)){
			$this->assertAttributeEquals($args, 'args', $this->cmd);
		}
	}

	/**
	 * @return array
	 */
	public function handleArgumentsProvider(): array{
		return [
			['gitv --option', [], GitVersionCommandException::UNRECOGNIZED_OPTION],
			['gitv init --option aaa', [], GitVersionCommandException::OPTION_AFTER_COMMAND],
			['gitv', [], null, '', []],
			['gitv cmd', [], GitVersionCommandException::NO_SUCH_COMMAND, '', []],
			['gitv init', [], null, 'init'],
			['gitv init', ['working-directory' => '/not-exists'], GitVersionCommandException::NO_SUCH_DIRECTORY],
		];
	}

	/**
	 * @dataProvider getConfigProvider
	 *
	 * @param string $expectedException
	 * @param callable|null $pre
	 */
	public function testGetConfig(string $expectedException = null, callable $pre = null){
		@unlink(
			dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest'.DIRECTORY_SEPARATOR
			.".gitv".DIRECTORY_SEPARATOR."config.json"
		);
		if(!is_null($pre)){
			$pre();
		}
		$this->cmd = new Command();
		TestsHelper::invokeMethod($this->cmd, 'handleArguments', [
			[], ['w' => dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest']
		]);
		if(!is_null($expectedException) && $expectedException !== ''){
			$this->expectException($expectedException);
		}
		$ret = TestsHelper::invokeMethod($this->cmd, 'getConfig');
		if($expectedException === ''){
			$this->assertFalse($ret);
		}else{
			$this->assertInstanceOf(GitVersionConfig::class, $ret);
		}
	}

	/**
	 * @return array
	 */
	public function getConfigProvider(): array{
		$gitvPath = dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest'.DIRECTORY_SEPARATOR.".gitv";
		return [
			[''],
			[\TypeError::class, function() use ($gitvPath){
				@mkdir($gitvPath);
				file_put_contents(
					$gitvPath.DIRECTORY_SEPARATOR."config.json",
					""
				);
			}],
			[\TypeError::class, function() use ($gitvPath){
				@mkdir($gitvPath);
				file_put_contents(
					$gitvPath.DIRECTORY_SEPARATOR."config.json",
					'{"dateFormat":[]}'
				);
			}],
			[null, function() use ($gitvPath){
				@mkdir($gitvPath);
				file_put_contents(
					$gitvPath.DIRECTORY_SEPARATOR."config.json",
					"[]"
				);
			}]
		];
	}
}
