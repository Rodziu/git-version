<?php
/**
 *    _______ __     _    __               _
 *   / ____(_) /_   | |  / /__  __________(_)___  ____
 *  / / __/ / __/   | | / / _ \/ ___/ ___/ / __ \/ __ \
 * / /_/ / / /_     | |/ /  __/ /  (__  ) / /_/ / / / /
 * \____/_/\__/     |___/\___/_/  /____/_/\____/_/ /_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GitVersion;

use PHPUnit\Framework\TestCase;
use Rodziu\Git\GitRepository;
use Rodziu\Types\GitVersionConfig;

/**
 * Class GitChangelogTest
 * @package Rodziu\GitVersion
 */
class GitChangelogTest extends TestCase{
	/**
	 * @var GitChangelog
	 */
	private $gc;

	/**
	 */
	protected function setUp(){
		parent::setUp();
		$wd = dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest';
		$this->gc = new GitChangelog(
			$wd, new GitRepository($wd.DIRECTORY_SEPARATOR.'.git'),
			new GitVersionConfig()
		);
	}

	/**
	 */
	protected function tearDown(){
		parent::tearDown();
		@unlink(dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest'.DIRECTORY_SEPARATOR.'changelog.md');
	}

	/**
	 * @dataProvider getChangelogFromCommitsProvider
	 *
	 * @param array $expected
	 * @param string|null $afterTag
	 * @param string|null $newVersion
	 */
	public function testGetChangelogFromCommits(array $expected, ?string $afterTag, string $newVersion = null){
		$changelog = $this->gc->getChangelogFromCommits($afterTag, $newVersion);
		$this->assertEquals($expected, $changelog);
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	public function getChangelogFromCommitsProvider(): array{
		return [
			'all'         => [[
				[
					'version' => '1.0.0',
					'authors' => ['Mateusz Rohde'],
					'text'    => ['branch', 'first commit line', 'second commit line', 'third commit line', 'master commit'],
					'date'    => new \DateTimeImmutable('2017-06-08 14:19:17', new \DateTimeZone('+02:00'))
				],
				[
					'version' => '0.2.0',
					'authors' => ['Mateusz Rohde'],
					'text'    => ["Merge branch 'someBranch'", 'second branch commit', 'first branch commit'],
					'date'    => new \DateTimeImmutable('2017-06-06 10:56:07', new \DateTimeZone('+02:00'))
				],
				[
					'version' => '0.1.0',
					'authors' => ['Mateusz Rohde'],
					'text'    => ['second master commit', 'first commit on master'],
					'date'    => new \DateTimeImmutable('2017-06-06 10:55:54', new \DateTimeZone('+02:00'))
				]
			], null],
			'after 0.2.0' => [[
				[
					'version' => '1.0.0',
					'authors' => ['Mateusz Rohde'],
					'text'    => ['branch', 'first commit line', 'second commit line', 'third commit line', 'master commit'],
					'date'    => new \DateTimeImmutable('2017-06-08 14:19:17', new \DateTimeZone('+02:00'))
				]
			], '0.2.0'],
			'after 1.0.0' => [[
				[
					'version' => '1.0.1',
					'authors' => ['Mateusz Rohde'],
					'text'    => ['commit', 'v 1.0.1'],
					'date'    => new \DateTimeImmutable('2017-06-09 11:42:28', new \DateTimeZone('+02:00'))
				]
			], '1.0.0', '1.0.1']
		];
	}

	/**
	 * @dataProvider getLastTagInChangelogProvider
	 *
	 * @param string $changelog
	 * @param string|null $expected
	 */
	public function testGetLastTagInChangelog(string $changelog, string $expected = null){
		$wd = dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest';
		file_put_contents($wd.DIRECTORY_SEPARATOR.'changelog.md', $changelog);
		$lastTag = $this->gc->getLastTagInChangelog();
		$this->assertEquals($expected, $lastTag);
	}

	/**
	 * @return array
	 */
	public function getLastTagInChangelogProvider(): array{
		return [
			['', null],
			["# 1.0.0\n\n{text}\n\n# 0.2.0\n\n{text}\n\n# 0.1.0\n\n{text}\n\n", '1.0.0'],
			["# 0.2.0\n\n{text}\n\n# 0.1.0\n\n{text}", '0.2.0']
		];
	}

	/**
	 */
	public function testUpdateChangelog(){
		$this->gc->updateChangelog();
		$changelog = dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest'.DIRECTORY_SEPARATOR.'changelog.md';
		$this->assertFileExists($changelog);
		$this->assertFalse($this->gc->hasNewChanges());
	}
}
