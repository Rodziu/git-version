<?php
/**
 *    _______ __     _    __               _
 *   / ____(_) /_   | |  / /__  __________(_)___  ____
 *  / / __/ / __/   | | / / _ \/ ___/ ___/ / __ \/ __ \
 * / /_/ / / /_     | |/ /  __/ /  (__  ) / /_/ / / / /
 * \____/_/\__/     |___/\___/_/  /____/_/\____/_/ /_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GitVersion;

use \PHPUnit\Framework\TestCase;
use Rodziu\Exception\GitVersionException;
use Rodziu\Git\GitRepository;
use Rodziu\Types\GitVersion;
use Rodziu\Types\GitVersionConfig;

/**
 * Class VersionTest
 * @package Rodziu\GitVersion
 */
class VersionTest extends TestCase{
	/**
	 * @var Version
	 */
	private $v;

	/**
	 */
	protected function setUp(){
		parent::setUp();
		$wd = dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest';
		$this->v = new Version(
			$wd, new GitRepository($wd.DIRECTORY_SEPARATOR.'.git'),
			new GitVersionConfig()
		);
	}

	/**
	 */
	protected function tearDown(){
		parent::tearDown();
		$wd = dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest';
		exec("cd $wd && git checkout master -q");
	}

	/**
	 * @dataProvider validateSemverProvider
	 *
	 * @param string $version
	 * @param bool $expected
	 */
	public function testValidateSemver(string $version, bool $expected){
		self::assertSame($expected, Version::validateSemver($version));
	}

	/**
	 * @return array
	 */
	public function validateSemverProvider(): array{
		return [
			['1', false],
			['1.1', false],
			['1.1.1', true],
			['1.0.0-alpha', true],
			['1.0.0-alpha.1', true],
			['1.0.0-0.3.7', true],
			['1.0.0-x.7.z.92', true],
			['1.0.0-alpha+001', true],
			['1.0.0+20130313144700', true],
			['1.0.0-beta+exp.sha.5114f85', true],
			['1.0.0-.123', false],
			['1.0.0-...', false],
			['1.0.0-123.', false],
			['1.0.0-+', false],
			['1.0.0-+123', false],
			['1.0.0-', false],
			['1.0.0+.123', false],
			['1.0.0+...', false],
			['1.0.0+123.', false],
			['1.0.0+', false],
		];
	}

	/**
	 * @dataProvider getVersionProvider
	 *
	 * @param string $checkout
	 * @param string $expected
	 * @param string|null $expectedBuild
	 */
	public function testGetVersion(string $checkout, string $expected = null, string $expectedBuild = null){
		$wd = dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest';
		exec("cd $wd && git checkout $checkout -q");
		if(is_null($expected)){
			$this->expectException(GitVersionException::class);
			$this->expectExceptionCode(GitVersionException::DETACHED_HEAD_STATE);
		}
		$version = $this->v->getVersion();
		if(!is_null($expected)){
			$this->assertEquals(new GitVersion($expected, $expectedBuild), $version);
		}
	}

	/**
	 * @return array
	 */
	public function getVersionProvider(): array{
		/** @noinspection SpellCheckingInspection */
		return [
			['master', 'dev-master', 20170609094228],
			['detachedBranch', 'dev-detachedBranch', 20170606090258],
			['someBranch', 'dev-someBranch', 20170606085516],
			['1.0.0', '1.0.0', 20170608121917],
			['8dfb1dd06eef93b66d5b42df8ade9662fa41b752', null, null],
			['bd5785f3aa2e35c60f70e4df8ef97613a43391b4', '0.1.0', 20170606085554],
			['b931c2fe4ad701ca4e4839ce5d729bdeb667e681', '1.0.0', 20170608121917]
		];
	}
}
