<?php
/**
 *    _______ __     _    __               _
 *   / ____(_) /_   | |  / /__  __________(_)___  ____
 *  / / __/ / __/   | | / / _ \/ ___/ ___/ / __ \/ __ \
 * / /_/ / / /_     | |/ /  __/ /  (__  ) / /_/ / / / /
 * \____/_/\__/     |___/\___/_/  /____/_/\____/_/ /_/
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GitVersion;

use \PHPUnit\Framework\TestCase;
use Rodziu\Exception\GitHooksException;

/**
 * Class GitHooksTest
 * @package Rodziu\GitVersion
 */
class GitHooksTest extends TestCase{
	/**
	 * @var GitHooks
	 */
	private $gh;

	/**
	 */
	protected function setUp(){
		parent::setUp();
		$this->gh = new GitHooks(dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest');
	}

	/**
	 */
	public function testGetRelativePathException(){
		$this->expectException(GitHooksException::class);
		$this->expectExceptionCode(GitHooksException::CALL_OUTSIDE_GITV);
		$this->gh->getRelativePath();
	}

	/**
	 */
	public function testGetRelativePath(){
		$gitv = dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.'gitv';
		define('GIT_VERSION_PATH', $gitv);
		$wd = dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest';
		$relativePath = $this->gh->getRelativePath();
		chdir($wd);
		$this->assertFileEquals($gitv, $relativePath);
	}

	/**
	 */
	public function testCreateHook(){
		$content = "some content\n content";
		$expected = '#!/bin/sh'.PHP_EOL."# begin git version".PHP_EOL.$content.PHP_EOL.'# end git version'.PHP_EOL;
		$test = dirname(__DIR__).DIRECTORY_SEPARATOR.'gitTest'.DIRECTORY_SEPARATOR.'.git'.DIRECTORY_SEPARATOR.'hooks'.DIRECTORY_SEPARATOR.'test';
		@unlink($test);
		$this->gh->createHook('test', $content);
		$assert = function() use ($test, &$expected){
			$this->assertFileExists($test);
			$this->assertEquals($expected, file_get_contents($test));
			$this->assertTrue(is_executable($test));
		};
		$assert();
		$this->gh->createHook('test', $content); // update
		$assert();
		/** @noinspection PhpUnusedLocalVariableInspection */
		$expected = '#!/bin/sh'.PHP_EOL."sth".PHP_EOL."# begin git version".PHP_EOL.$content.PHP_EOL.'# end git version'.PHP_EOL;
		file_put_contents($test, '#!/bin/sh'.PHP_EOL."sth");
		$this->gh->createHook('test', $content);
		$assert();
		$this->gh->createHook('test', $content); // update
		$assert();
		@unlink($test);
	}
}
